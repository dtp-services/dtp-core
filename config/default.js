const path = require('path');
const env = process.env;

const gateway = env.GATEWAY || '172.24.0.1';

const DB_PREFIX = env.DB_PREFIX || '';
const MONGO_HOST = gateway;
const MONGO_PORT = env.MONGO_PORT || 27017;
const PORT = require('./port');
const ENV = env.NODE_ENV || 'development';

const MONGO_CONF_DEFAULT = {
  host: MONGO_HOST,
  port: MONGO_PORT,
  log: process.env.LOG_LEVEL || 'error'
};

const httpsMode = ENV === 'production' && !process.env.NOHTTPS;

module.exports = {
  // support protocol
  proto: httpsMode ? 'https://' : 'http://',
  // domains of services
  host: {
    main: 'dtp.pw',
    api: 'api.dtp.pw',
    files: 'files.dtp.pw',
    tph: 'tph.su',
    cgnode: 'cgnode.dtp.pw',
    cgphp: 'cgphp.dtp.pw'
  },
  gateway,
  // local links of services
  local: {
    main: gateway + ':' + PORT['dtp-front'][ENV],
    api: gateway + ':' + PORT['dtp-api'][ENV],
    files: gateway + ':' + PORT['telegram-files'][ENV],
    admin: gateway + ':' + PORT['dtp-admin'][ENV],
    tph: gateway + ':' + PORT['dtp-link-allocator'][ENV],
    textValidator: gateway + ':' + PORT['dtp-text-validator'][ENV],
    telegraphAggregator: gateway + ':' + PORT['telegraph-aggregator'][ENV],
    cgnode: gateway + ':' + PORT['cover-generator'][ENV],
    cgphp: gateway + ':' + PORT['cover-generator-php'][ENV]
  },
  // bot's info
  bot: {
    publisher: {
      username: '',
      token: ''
    },
    adminNotify: {
      username: '',
      token: ''
    }
  },
  //Another services
  services: {
    yandex: {
      // api key for yandex translate
      apiKey: ''
    }
  },
  // ports of services
  port: PORT,
  // db groups
  db: {
    'mongo-users': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-users',
      models: require('./mongo-models/mongo-users')
    }),
    'mongo-content': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-content',
      models: require('./mongo-models/mongo-content')
    }),
    'mongo-telegraph-accounts': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-telegraph-accounts',
      models: require('./mongo-models/mongo-telegraph-accounts')
    }),
    'mongo-front': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-front',
      models: require('./mongo-models/mongo-front')
    }),
    'mongo-admin': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-admin',
      models: require('./mongo-models/mongo-admin')
    }),
    'mongo-pub-bot': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-pub-bot',
      models: require('./mongo-models/mongo-pub-bot')
    }),
    'mongo-stats': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-stats',
      models: require('./mongo-models/mongo-stats')
    }),
    'mongo-service': Object.assign({}, MONGO_CONF_DEFAULT, {
      db: DB_PREFIX + 'mongo-service',
      models: require('./mongo-models/mongo-service')
    })
  },
  mongo: {
    host: MONGO_HOST,
    port: MONGO_PORT
  },
  workdir: path.resolve(__dirname, '../../')
};