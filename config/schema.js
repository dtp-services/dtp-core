class Schema {
  static get user_id () {
    return {
      type: 'number',
      minimum: 1
    };
  }

  static get authorUsername () {
    return {
      type: 'string',
      pattern: '^([a-zA-Z0-9_]+)$',
      minLength: 2,
      maxLength: 24
    }
  }

  static get userPhoto () {
    return {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          file_id: Schema.anyString,
          file_size: Schema.anyNumber,
          width: Schema.anyNumber,
          height: Schema.anyNumber
        },
        required: ['file_id']
      },
      minItems: 1
    }
  }

  static get boolean () {
    return {
      type: 'boolean'
    };
  }

  static get access_mask () {
    return {
      type: 'object',
      properties: {
        PFSB: Schema.boolean, // Publish from Sandbox
        DFDTP: Schema.boolean, // Delete from DTP
        EAI: Schema.boolean, // Edit account info
        DOM: Schema.boolean // Delete other members
      },
      required: ['PFSB', 'DFDTP', 'EAI', 'DOM']
    };
  }

  static get short_id () {
    return {
      type: 'string',
      maxLength: 15
    };
  }

  static get path () {
    return {
      type: 'string'
    }
  }

  static get telegraphURL () {
    return {
      type: 'string',
      pattern: '^http(s)?:\\/\\/telegra\\.ph\\/[a-zA-Z-0-9%\\\W]+$'
    }
  }

  static get views () {
    return {
      type: 'number',
      minimum: 0
    }
  }

  static get telegraphFileURL () {
    return {
      type: 'string',
      pattern: '^http(s)?:\\/\\/telegra\\.ph\\/file\\/(\w)+\\.(\w)+$'
    }
  }

  static get anyString () {
    return {
      type: 'string'
    }
  }

  static get anyNumber () {
    return {
      type: 'number'
    }
  }

  static get anyURL () {
    return {
      type: 'string',
      pattern: '^(http(s)?|.+)?:\\/\\/.+$'
    }
  }

  static get preview () {
    return {
      type: 'object',
      properties: {
        path: Schema.path,
        url: Schema.telegraphURL,
        title: Schema.anyString,
        description: Schema.anyString,
        views: Schema.views,
        image_url: Schema.anyURL,
        author_name: Schema.anyString,
        author_url: Schema.anyURL,
        updatedDate: Schema.anyNumber
      },
      required: ['url', 'path', 'title', 'description', 'views']
    }
  }

  static get tag () {
    return {
      type: 'string',
      pattern: '^#?([а-яА-Я\\w0-9])+$'
    }
  }

  static get tags () {
    return {
      type: 'array',
      items: Schema.tag,
      minItems: 1,
      maxItems: 10
    }
  }

  static get language () {
    return {
      type: 'string',
      enum: ['en', 'ru', 'uk', 'fr', 'it', 'es', 'nl', 'other', 'all']
    }
  }

  static get languages () {
    return {
      type: 'array',
      items: Schema.language,
      minItems: 0,
      maxItems: 10
    }
  }

  static get uuid () {
    return {
      type: 'string',
      pattern: '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$'
    }
  }

  static get votes () {
    return {
      type: 'object',
      properties: {
        up: Schema.anyNumber,
        down: Schema.anyNumber,
        total: Schema.anyNumber
      },
      required: ['up', 'down', 'total']
    }
  }

  static get anyObject () {
    return {
      type: 'object'
    }
  }

  static get voteDelta () {
    return {
      type: 'number',
      enum: [1, -1]
    }
  }
}

module.exports = Schema;
