const models = {};
const Schema = require('../schema');

models['posts'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      shortId: Schema.short_id,
      path: Schema.path,
      preview: Schema.preview,
      tags: Schema.tags,
      tags_index: Schema.anyString,
      language: Schema.language,
      controlKey: Schema.uuid,
      createdDate: Schema.anyNumber,
      votes: Schema.votes
    },
    required: ['path', 'tags', 'language', 'votes']
  }
};

models['contentIndex'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      createdDate: Schema.anyNumber,
      shortId: Schema.short_id,
      content: Schema.anyString
    },
    required: ['createdDate', 'shortId', 'content']
  }
};

models['authorNamesSandbox'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      username: Schema.authorUsername,
      createdDate: Schema.anyNumber
    },
    required: ['user_id', 'username', 'createdDate']
  }
};

models['contentCache'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      path: Schema.path,
      content: {
        type: 'array'
      },
      time: Schema.anyNumber,
      date: {
        bsonType: 'date'
      }
    },
    required: ['path', 'content', 'time', 'date']
  }
};

models['contentBlacklist'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      type: Schema.anyString,
      value: Schema.anyString
    },
    required: ['type', 'value']
  }
};

models['postCovers'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      sourceFile: Schema.anyURL,
      coverFile: Schema.anyURL,
      contentHash: Schema.anyString,
      createdDate: Schema.anyNumber
    },
    required: ['sourceFile', 'coverFile', 'contentHash', 'createdDate']
  }
};

module.exports = models;