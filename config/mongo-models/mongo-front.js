const models = {};
const Schema = require('../schema');

models['sessions'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      sessionid: Schema.anyString,
      data: Schema.anyObject
    },
    required: ['sessionid', 'data']
  }
};

module.exports = models;