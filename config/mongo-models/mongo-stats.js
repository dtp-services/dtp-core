const models = {};
const Schema = require('../schema');

models['rates'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      shortId: Schema.short_id,
      delta: Schema.voteDelta,
      date: Schema.anyNumber
    },
    required: ['user_id', 'shortId', 'delta', 'date']
  }
};

module.exports = models;