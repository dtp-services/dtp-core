const models = {};
const Schema = require('../schema');

models['banList'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      path: Schema.anyString,
      userFrom: Schema.user_id,
      reason: Schema.anyString,
      createdDate: Schema.anyNumber,
      blockAuthor: {
        type: 'array',
        items: Schema.user_id
      }
    },
    required: ['path', 'userFrom', 'reason', 'createdDate']
  }
};

module.exports = models;