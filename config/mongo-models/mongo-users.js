const models = {};
const Schema = require('../schema');

models['authKeys'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      openKey: Schema.anyString,
      openToken: Schema.anyString,
      date: {
        bsonType: 'date'
      }
    },
    required: ['openKey', 'openToken', 'date']
  }
};

models['users'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      id: Schema.user_id,
      first_name: Schema.anyString,
      last_name: Schema.anyString,
      full_name: Schema.anyString,
      photo: Schema.userPhoto,
      last_bot_session: Schema.anyNumber,
      last_api_session: Schema.anyNumber,
      secretToken: Schema.anyString,
      openToken: Schema.anyString,
      regtime: Schema.anyNumber
    },
    required: ['id', 'first_name', 'last_name', 'regtime']
  }
};

models['trendingFilters'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      language: Schema.languages,
      tags: {
        type: 'array',
        items: Schema.tag,
        minItems: 0,
        maxItems: 10
      }
    },
    required: ['user_id', 'language', 'tags']
  }
};

module.exports = models;