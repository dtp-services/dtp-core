const models = {};
const Schema = require('../schema');

models['gaSandbox'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      account_id: Schema.uuid,
      token: Schema.anyString
    },
    required: ['account_id', 'token']
  }
};

models['gaSandboxLinks'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      sandboxPath: Schema.anyString,
      originalPath: Schema.anyString
    },
    required: ['sandboxPath', 'originalPath']
  }
};

models['groupAccountsMembers'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      owner: Schema.boolean,
      access_mask: Schema.access_mask
    },
    required: ['user_id', 'account_id', 'owner']
  }
};

models['groupAccountsInvites'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      id: Schema.uuid,
      account_id: Schema.uuid
    },
    required: ['id', 'account_id']
  }
};

models['telegraphTokens'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      user_id: Schema.user_id,
      account_id: Schema.uuid,
      token: Schema.anyString,
      sandboxToken: Schema.anyString,
      createdDate: Schema.anyNumber
    },
    required: ['user_id', 'account_id', 'token', 'sandboxToken', 'createdDate']
  }
};

models['groupAccounts'] = {
  $jsonSchema: {
    bsonType: 'object',
    properties: {
      id: Schema.uuid,
      username: Schema.authorUsername,
      name: {
        type: 'string',
        maxLength: 128
      },
      token: Schema.anyString,
      createdDate: Schema.anyNumber
    },
    required: ['id', 'name', 'createdDate', 'token']
  }
};

module.exports - models;