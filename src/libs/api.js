const $request = require('request');
const $Promise = require('bluebird');
const $config = require('config');

class API {
  static get SERVER () {
    return 'http://' + $config.get('local.api') + '/';
  }

  static async query (method, params) {
    const query = await new $Promise((resolve, reject) => {
      $request({
        method: 'post',
        url: this.SERVER + method,
        json: params || true
      }, (err, response, body) => {
        if (err) {
          return reject(err);
        }

        resolve(body);
      });
    });

    return query;
  }
}

module.exports = API;