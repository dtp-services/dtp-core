const config = require('config');
const connectionConfig = config.get('db.mongo-telegraph-accounts');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  gaSandbox: mongo.collection('gaSandbox'),
  gaSandboxLinks: mongo.collection('gaSandboxLinks'),
  groupAccountsMembers: mongo.collection('groupAccountsMembers'),
  groupAccountsInvites: mongo.collection('groupAccountsInvites'),
  telegraphTokens: mongo.collection('telegraphTokens'),
  authorNamesSandbox: mongo.collection('authorNamesSandbox'),
  groupAccounts: mongo.collection('groupAccounts')
};