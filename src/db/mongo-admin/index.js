const config = require('config');
const connectionConfig = config.get('db.mongo-admin');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  banList: mongo.collection('banList')
};