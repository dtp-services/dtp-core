const config = require('config');
const connectionConfig = config.get('db.mongo-stats');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  rates: mongo.collection('rates')
};