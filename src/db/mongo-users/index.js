const config = require('config');
const connectionConfig = config.get('db.mongo-users');
const jMongo = require('just-mongo');

const mongo = new jMongo(connectionConfig);

module.exports = {
  users: mongo.collection('users'),
  authKeys: mongo.collection('authKeys'),
  trendingFilters: mongo.collection('trendingFilters')
};