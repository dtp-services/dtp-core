# DTP Core

## Install packages

- To install all ecosystem to run `./install/ecosystem.sh`
- Else use any of bash script in `./install`.

## Init system

- Creacte configuration file: `./config/{{ENV}}.json`. Connect to configuration file `./config/default.json` and set changes.

- Use `npm run build` to build ecosystem.
- For production build use `npm run build-prod`.

## Run any of services

**Run only stuff services**

```bash
STAFF_ONLY=true npm run build
npm start
```

**To restart services, use**

```bash
npm restart
```

**To debug mode of services**

```bash
DEBUG=true npm run build
npm start
```

## More ENV for build config

| Name       | Description                    |
|------------|--------------------------------|
| DB_PREFIX  | Prefix for db names            |
| LOG_LEVEL  | Log level                      |
| GATEWAY    | IP address to main network     |
| MONGO_PORT | Port to MongoDB instance       |
| WORKDIR    | Set workdir where all services |
| NOHTTPS    | Use no-https mode in production |