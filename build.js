const config = require('config');
const fs = require('fs');
const path = require('path');
const rmrf = require('rimraf');
const regExpEscape = require('escape-string-regexp');
const cpFile = require('cp-file');
const enfscopy = require('enfscopy');
const yamljs = require('yamljs');
const merge = require('lodash/merge');

const DIST_DIR = path.resolve(__dirname, 'dist');
const CONF_NGINX_DIR = path.resolve(__dirname, 'config/nginx');
const CONF_COMPOSE_DIR = path.resolve(__dirname, 'config/compose');
const CONF_PHP_DIR = path.resolve(__dirname, 'config/php');
const ENV = process.env.NODE_ENV || 'development';
const IS_DEBUG = process.env.DEBUG;
const WORKDIR = process.env.WORKDIR || config.get('workdir');
const GATEWAY = config.get('gateway');
// for build only: nginx, mongodb, etc. Without node services.
const STAFF_ONLY = Boolean(process.env.STAFF_ONLY);

let runMode;

switch (ENV) {
  case 'development':
    runMode = 'node-dev';
  break;
  case 'production':
    runMode = 'node'
  break;
}

if (IS_DEBUG) {
  runMode = 'debug';
}

const needLog = !IS_DEBUG;

async function buildNginxConfig () {
  console.log('Build nginx config');
  fs.mkdirSync(path.resolve(DIST_DIR, 'nginx'));
  fs.mkdirSync(path.resolve(DIST_DIR, 'nginx/servers'));

  const serversFiles = {
    'main.conf': {
      DOMAIN: config.get('host.main'),
      PORT: config.get('port.dtp-front.' + ENV),
      GATEWAY
    }, 
    'api.conf': {
      DOMAIN: config.get('host.api'),
      PORT: config.get('port.dtp-api.' + ENV),
      GATEWAY
    },
    'files.conf': {
      DOMAIN: config.get('host.files'),
      PORT: config.get('port.telegram-files.' + ENV),
      GATEWAY
    },
    'cgnode.conf': {
      DOMAIN: config.get('host.cgnode'),
      PORT: config.get('port.cover-generator.' + ENV),
      GATEWAY
    },
    'cgphp.conf': {
      DOMAIN: config.get('host.cgphp'),
      PORT: config.get('port.cover-generator-php.' + ENV),
      WORKDIR,
      GATEWAY
    }
  };

  const copyFiles = ['nginx.conf', 'mime.types', 'fastcgi.conf', 'fastcgi_params'];

  Object.keys(serversFiles).forEach((fileName) => {
    const vars = serversFiles[fileName];
    const filePath = path.join(CONF_NGINX_DIR, 'servers', fileName);
    let fileData = fs.readFileSync(filePath, {
      encoding: 'utf8'
    });
    const distServersFilePath = path.resolve(DIST_DIR, 'nginx/servers', fileName);

    fileData = Object.keys(vars).reduce((file, _var) => {
      file = file.replace(new RegExp(`{{${regExpEscape(_var)}}}`, 'g'), vars[_var]);
      return file;
    }, fileData);

    fs.writeFileSync(distServersFilePath, fileData);
  });

  for (const file of copyFiles) {
    await cpFile(path.resolve(CONF_NGINX_DIR, file), path.join(DIST_DIR, 'nginx', file));
  }

  console.log('Done.');
}

async function clearDist () {
  console.log('Clear dist');
  await new Promise((resolve) => rmrf(DIST_DIR, resolve));
  fs.mkdirSync(DIST_DIR);
  console.log('Done.');
}

async function buildComposeFiles () {

  console.log('Build docker-compose config');
  fs.mkdirSync(path.resolve(DIST_DIR, 'compose'));

  const composeFiles = {
    'nginx.yml': {
      WORKDIR
    },
    'mongo.yml': {
      WORKDIR,
      MONGO_PORT: config.get('mongo.port'),
    },
    'node.yml': {
      WORKDIR,
      run_mode: runMode,
      return_log: needLog && '&& pm2 logs' || '',
      // ports
      api_port: config.get('port.dtp-api.' + ENV),
      front_port: config.get('port.dtp-front.' + ENV),
      tg_fs_port: config.get('port.telegram-files.' + ENV),
      dtp_txt_valid: config.get('port.dtp-text-validator.' + ENV),
      tgph_agg: config.get('port.telegraph-aggregator.' + ENV),
      cvr_gen: config.get('port.cover-generator.' + ENV),
      dtp_lnk_alloc: config.get('port.dtp-link-allocator.' + ENV),
      dtp_admin: config.get('port.dtp-admin.' + ENV),
    },
    'networks.yml': {}
  };

  const publicCompose = {};
  const distPublicComposeFilePath = path.resolve(DIST_DIR, 'compose', 'public-compose.yml');

  Object.keys(composeFiles).forEach((fileName) => {
    if (fileName === 'node.yml' && STAFF_ONLY) {
      return;
    }

    const vars = composeFiles[fileName];
    const filePath = path.join(CONF_COMPOSE_DIR, fileName);
    let fileData = fs.readFileSync(filePath, {
      encoding: 'utf8'
    });
    const distComposeFilePath = path.resolve(DIST_DIR, 'compose', fileName);

    fileData = Object.keys(vars).reduce((file, _var) => {
      file = file.replace(new RegExp(`{{${regExpEscape(_var)}}}`, 'g'), vars[_var]);
      return file;
    }, fileData);

    fs.writeFileSync(distComposeFilePath, fileData);
    merge(publicCompose, yamljs.parse(fileData));
  });
  
  const publicComposeYML = yamljs.stringify(publicCompose);

  fs.writeFileSync(distPublicComposeFilePath, publicComposeYML);

  console.log('Done.');
}

async function buildPHPConfig () {
  console.log('Build php config');

  const DIST_PHP_DIR = path.resolve(DIST_DIR, 'php');

  enfscopy.copySync(CONF_PHP_DIR, DIST_PHP_DIR);
}

async function main () {
  await clearDist();
  await buildNginxConfig();
  await buildComposeFiles();
  await buildPHPConfig();
};

main().catch((err) => {
  console.log(err);
});